Dockerfiles
===========

This is a set of Docker files for building images used for building and running
tests.  To minimize overall space, the images build on each other to add
functionality.

If you have push access to the `lv2plugin` organization on Dockerhub, simply
running `make` in this directory will build and push all the images.
Otherwise, you will need to modify things to use your own Docker repository.

 -- David Robillard <d@drobilla.net>
