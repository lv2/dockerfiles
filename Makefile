all: build

build:
	docker build -t lv2plugin/debian debian
	docker build -t lv2plugin/debian-dev debian-dev
	docker build -t lv2plugin/debian-arm32 debian-arm32
	docker build -t lv2plugin/debian-arm64 debian-arm64
	docker build -t lv2plugin/debian-mingw32 debian-mingw32
	docker build -t lv2plugin/debian-mingw64 debian-mingw64
	docker build -t lv2plugin/debian-wasm debian-wasm
	docker build -t lv2plugin/debian-x32 debian-x32
	docker build -t lv2plugin/debian-x64 debian-x64
	docker build -t lv2plugin/debian-x64-big debian-x64-big
	docker build -t lv2plugin/debian-x64-clang debian-x64-clang
	docker build -t lv2plugin/fedora-dev fedora-dev
	docker build -t lv2plugin/fedora-big fedora-big

push:
	docker push lv2plugin/debian
	docker push lv2plugin/debian-dev
	docker push lv2plugin/debian-arm32
	docker push lv2plugin/debian-arm64
	docker push lv2plugin/debian-mingw32
	docker push lv2plugin/debian-mingw64
	docker push lv2plugin/debian-wasm
	docker push lv2plugin/debian-x32
	docker push lv2plugin/debian-x64
	docker push lv2plugin/debian-x64-big
	docker push lv2plugin/debian-x64-clang
	docker push lv2plugin/fedora-dev
	docker push lv2plugin/fedora-big
